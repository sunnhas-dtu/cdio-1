package spil;

public class Game {
    private Player[] players;
    private int playerIndex = 0;

    private DiceCup dies;


    public Game(Player p1, Player p2)
    {
        this.players = new Player[2];
        this.players[0] = p1;
        this.players[1] = p2;

        this.dies = new DiceCup();
    }


    public void play()
    {
        this.printWelcomeMessage();
        this.printRules();
        this.print("We are now starting out.");

        Player currentPlayer;
        boolean winnerFound = false;

        do {
            currentPlayer = this.getNextPlayer();

            this.print("");
            this.print("It's " + currentPlayer.getName() + "'s turn.");

            do {
                this.dies.roll();

                this.print("You rolled " + this.dies.getDice1Value() + " and " + this.dies.getDice2Value());
                this.print("That sums to " + this.dies.getSum());

                currentPlayer.addScore(this.dies.getSum());

                if (this.dies.isPairOne()) {
                    this.print("Sorry, but you rolled a pair 1...");
                    currentPlayer.resetScore();
                }

                this.print("Your total score is now " + currentPlayer.getScore());

                if (
                    (this.dies.isPairTwelve() && currentPlayer.getLastRollValue() == 12)
                    || (currentPlayer.getScore() >= 40 && this.dies.isPair())
                ) {
                    winnerFound = true;
                    break;
                } else if (this.dies.isPair()) {
                    this.print("You rolled a pair - roll again!");
                }

                currentPlayer.setLastRollValue(this.dies.getSum());
            } while (this.dies.isPair());

        } while (! winnerFound);

        this.printScoreBoard(currentPlayer);
    }


    private Player getNextPlayer()
    {
        if (this.playerIndex >= this.players.length) {
            this.playerIndex = 0;
        }

        return this.players[this.playerIndex++];
    }


    private void printWelcomeMessage()
    {
        this.print("Welcome to the players:");
        for (Player player : this.players) {
            this.print(" - " + player.getName());
        }
        this.print("");
    }


    private void printRules()
    {
        this.print("The rules:");
        this.print("Each player rolls with 2 dies.");
        this.print("The sum of the dies is added to the players score.");
        this.print("When the player reach 40 points, he/she has to roll a pair to win.");
        this.print("If a player rolls a pair 1, his/hers score is reset.");
        this.print("If a player rolls a pair, he/she gets another turn.");
        this.print("If a player rolls a pair 6 after another pair 6, he/she wins the game.");
        this.print("");
    }


    private void printScoreBoard(Player winningPlayer)
    {
        this.print("");

        this.print("--- The scoreboard ---");
        for (Player player : this.players) {
            System.out.print(" - " + player.getName() + ": " + player.getScore());

            if (player == winningPlayer) {
                System.out.print(" (the winner)");
            }

            this.print("");
        }

        this.print("");
        this.print("And the winner is " + winningPlayer.getName());
    }


    private void print(String msg)
    {
        System.out.println(msg);
    }
}
