package spil;

public class Player {
    private String name;
    private int score;
    private int lastRollValue;


    public Player(String name)
    {
        this.name = name;
    }


    public String getName()
    {
        return this.name;
    }


    public int getScore()
    {
        return score;
    }


    public void addScore(int numberToAdd)
    {
        this.score += numberToAdd;
    }


    public void resetScore()
    {
        this.score = 0;
    }


    public void setLastRollValue(int lastRollValue)
    {
        this.lastRollValue = lastRollValue;
    }


    public int getLastRollValue()
    {
        return this.lastRollValue;
    }
}
